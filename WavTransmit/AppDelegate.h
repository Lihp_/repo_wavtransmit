//
//  AppDelegate.h
//  WavTransmit
//
//  Created by Igounorca on 2016/2/19.
//  Copyright © 2016年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

