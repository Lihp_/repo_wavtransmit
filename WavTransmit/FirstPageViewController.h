//
//  ViewController.h
//  WavTransmit
//
//  Created by Igounorca on 2016/2/19.
//  Copyright © 2016年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstPageViewController : UIViewController

@property UIButton *SendBtn;
@property UIButton *ReceiveBtn;

-(void)SegueToSend:(id)sender;
-(void)SegueToReceive:(id)sender;

@end

