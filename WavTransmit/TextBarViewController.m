//
//  TextBar.m
//  WavTransmit
//
//  Created by Igounorca on 2016/2/19.
//  Copyright © 2016年 Igounorca. All rights reserved.
//

#import "TextBarViewController.h"
#import "EZAudio.h"

#define SAMPLE_RATE 44100

@interface TextBarViewController ()
<EZOutputDataSource,
 EZOutputDelegate>

@property double theta;
@property double frequency;
@property Float32 amplitude;
@property double sampleRate;
@property AudioBufferList audioBufferList;
@property const AudioTimeStamp timestamp;

@end

@implementation TextBarViewController

@synthesize textField,TransmitBtn,String,rioUnit,audioFormat,audioBufferList,timestamp;

-(void)textFieldDone:(UITextField*)textField
{
    [textField resignFirstResponder];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    TransmitBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [TransmitBtn setTitle:@"Send" forState:UIControlStateNormal];
    [TransmitBtn setFrame:CGRectMake(150, 250, 100, 50)];
    [TransmitBtn addTarget:self action:@selector(Transmit) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:TransmitBtn];
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(100 ,150 ,200 , 20)];
    textField.borderStyle=UITextBorderStyleRoundedRect;
    textField.placeholder=@"Type the word you want";
    textField.clearsOnBeginEditing=YES;
    textField.autocapitalizationType=UITextAutocapitalizationTypeWords;
    textField.keyboardType=UIKeyboardTypeDefault;
    textField.returnKeyType=UIReturnKeySend;
    [self.view addSubview:textField];
    [textField addTarget:self
               action:@selector(textFieldDone:)
               forControlEvents:UIControlEventEditingDidEndOnExit];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)CreateEZOutput
{
    //
    // Create EZOutput to play audio data with mono format (EZOutput will convert
    // this mono, float "inputFormat" to a clientFormat, i.e. the stereo output format).
    //
    AudioStreamBasicDescription inputFormat = [EZAudioUtilities monoFloatFormatWithSampleRate:SAMPLE_RATE];
    self.output = [EZOutput outputWithDataSource:self inputFormat:inputFormat];
    [self.output setDelegate:self];
    //self.frequency = 200.0;
    self.sampleRate = SAMPLE_RATE;
    self.amplitude = 0.80;
}

- (OSStatus)        output:(EZOutput *)output
 shouldFillAudioBufferList:(AudioBufferList *)audioBufferList
        withNumberOfFrames:(UInt32)frames
                 timestamp:(const AudioTimeStamp *)timestamp
{
    Float32 *buffer = (Float32 *)audioBufferList->mBuffers[0].mData;
    size_t bufferByteSize = (size_t)audioBufferList->mBuffers[0].mDataByteSize;
    double theta = self.theta;
    double frequency = self.frequency;
    double thetaIncrement = 2.0 * M_PI * frequency / SAMPLE_RATE;
    for (UInt32 frame = 0; frame < frames; frame++)
    {
        buffer[frame] = self.amplitude * sin(theta);
        theta += thetaIncrement;
        if (theta > 2.0 * M_PI)
        {
            theta -= 2.0 * M_PI;
        }
    }
    self.theta = theta;
    
    return noErr;
}

- (void)Transmit{
    
    int i;
    
    String = textField.text;
    
    unichar Note[String.length];
    unichar Freq[String.length];
    
    [self CreateEZOutput];
    audioBufferList.mBuffers[0].mData = &(audioBufferList);
    audioBufferList.mBuffers[0].mDataByteSize = sizeof(float) * SAMPLE_RATE;
    
    for(i=0; i<String.length; i++){
        
        Note[i] = [String characterAtIndex:i];
        Freq[i] = [self ASCIIToNoteFreq:Note[i]];
        
        self.frequency = Freq[i];
        
        [self output:self.output shouldFillAudioBufferList:&(audioBufferList) withNumberOfFrames:SAMPLE_RATE timestamp:&(timestamp)];
        
        [self.output startPlayback];
    }
    

}

-(int)ASCIIToNoteFreq:(unichar)Note{
    
    return (440*pow(2, ((Note-69)/12)));   //(2^((Note-69)/12))
    
}

/*-(void)initaudioUnit{
    
    AudioComponentDescription Description;
    Description.componentType = kAudioUnitType_Output;
    Description.componentSubType = kAudioUnitSubType_RemoteIO;
    Description.componentManufacturer = kAudioUnitManufacturer_Apple;
    Description.componentFlags = 0;
    Description.componentFlagsMask = 0;
    
    // Get the default playback output unit
    AudioComponent OutputComponent = AudioComponentFindNext(NULL, &Description);
    NSAssert(OutputComponent, @"Can't find default output");
    
    // Create a new unit based on this that we'll use for output
    OSErr err = AudioComponentInstanceNew(OutputComponent, &rioUnit);
    NSAssert1(rioUnit, @"Error creating unit: %hd", err);
    
    audioFormat.mSampleRate= 44100.0;
    audioFormat.mFormatID= kAudioFormatLinearPCM;
    audioFormat.mFormatFlags= kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    audioFormat.mFramesPerPacket= 1;
    audioFormat.mChannelsPerFrame= 1;
    audioFormat.mBitsPerChannel= 16;
    audioFormat.mBytesPerPacket= 2;
    audioFormat.mBytesPerFrame= 2;

}*/
@end
