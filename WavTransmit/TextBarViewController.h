//
//  header.h
//  WavTransmit
//
//  Created by Igounorca on 2016/2/19.
//  Copyright © 2016年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <math.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "EZAudio.h"

@interface TextBarViewController : UIViewController

@property UIButton *TransmitBtn;
@property UITextField *textField;

@property NSString *String;

@property (nonatomic, assign) AudioStreamBasicDescription audioFormat;
@property (nonatomic, assign) AudioUnit rioUnit;

@property (nonatomic, strong) EZOutput *output;
@property (nonatomic, strong) EZAudioUtilities *utilities;

- (void)Transmit;
- (int)ASCIIToNoteFreq:(unichar)Note;
- (void)CreateEZOutput;
- (OSStatus)output:(EZOutput *)output
            shouldFillAudioBufferList:(AudioBufferList *)audioBufferList
            withNumberOfFrames:(UInt32)frames
            timestamp:(const AudioTimeStamp *)timestamp;


@end