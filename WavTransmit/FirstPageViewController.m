//
//  ViewController.m
//  WavTransmit
//
//  Created by Igounorca on 2016/2/19.
//  Copyright © 2016年 Igounorca. All rights reserved.
//

#import "FirstPageViewController.h"

@interface FirstPageViewController ()

@end

@implementation FirstPageViewController

@synthesize SendBtn,ReceiveBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SendBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [SendBtn setTitle:@"Send" forState:UIControlStateNormal];
    [SendBtn setFrame:CGRectMake(75, 300, 100, 50)];
    [SendBtn addTarget:self action:@selector(SegueToSend:) forControlEvents:UIControlEventTouchUpInside];
    
    ReceiveBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [ReceiveBtn setTitle:@"Receive" forState:UIControlStateNormal];
    [ReceiveBtn setFrame:CGRectMake(250, 300, 100, 50)];
    [ReceiveBtn addTarget:self action:@selector(SegueToReceive:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:SendBtn];
    [self.view addSubview:ReceiveBtn];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)SegueToSend:(id)sender{
    [self performSegueWithIdentifier:@"TextBarSegue" sender:sender];
}

- (void)SegueToReceive:(id)sender{
    
}
@end
